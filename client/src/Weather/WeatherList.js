import React, {useState} from 'react';
import axios from "axios";


function Weather() {

    const [weather, setWeather] = useState([{ 
        date: "",
        temperatureC: 0,
        temperatureF: 0,
        summary: "",
      }]);

    function getW() {
        axios.get('https://localhost:5001/weatherforecast')
        .then((response) => {
            setWeather(response.data);
            console.log("Weather =", weather);
        })

    }

    return(
        <div>
            <button onClick = {getW} >жми</button>
            <ul>
                {weather && weather.map(function(el,index) {
                    return <li key={index}>{"date: " + el.date + " ---> temperatureC: " + el.temperatureC+ " ---> temperatureF: " + el.temperatureF+ " ---> summary: " + el.summary}</li>
                })}
            </ul>
        </div>
    );

}
export default Weather
